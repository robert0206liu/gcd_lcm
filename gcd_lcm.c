#include <stdio.h> 
#include <stdlib.h> 

int Gcd(int a, int b)
{
	int c = 0;
	
	while(b != 0) { 
        c = a % b;
        a = b; 
        b = c; 
    } 
    return a;
}

int GcdR(int a, int b)
{
	if(b == 0)
	{
		return a;
	}
	
	return GcdR(b, a%b);
}

int Lcm(int a, int b) 
{
    return a * b / Gcd(a, b);
}

int main(void) { 
    int a = 0;
	int b = 0; 

    printf("Please input value 1: "); 
    scanf("%d", &a); 
	printf("Please input value 2: "); 
    scanf("%d", &b); 

    printf("Gcd:  %d\n", Gcd(a, b));
	printf("GcdR: %d\n", GcdR(a, b));
    printf("Lcm:  %d\n", Lcm(a, b));
    
    return 0; 
} 